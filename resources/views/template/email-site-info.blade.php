<div class="row">
    @forelse($dataList as $key => $record)
        @php
            $categories = getCategories($record['description'], $record['title']);
            $categories = empty($categories) ? [] : $categories;
            $emails = extractEmailsFrom($record['title'] . $record['description']);
            if(!@$emails[0]){
                continue;
            }
        @endphp

        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 class="text-primary" style="margin-top:0px;font-size: 18px;">
                        <a href="{{route('outlink', ['website' => $record['link']])}}"  target="_blank"
                           rel="nofollow noopener noreferrer">{!!  $record['title']  !!}</a>
                    </h3>
                    <p>
                        @if(@$emails[0])
                            <button class="btn btn-warning pull-left" style="margin-right:10px">
                                <a class="email" href="mailto:{{ @$emails[0] }}"  rel="nofollow noopener noreferrer"
                                   style="font-size: 14px"><i class="fa fa-envelope-o"></i> {{ @$emails[0]}}</a>
                            </button>
                        @endif
                    {!! getBoldStringHtml($record['description'], [@$keyword, @$emails[0]]) !!}
                    </p>
                    <p>
                        @if(count($categories) > 0)
                            @foreach($categories as $k => $cat)
                                <a href="{{route('keyword.index', getSlug($cat))}}" class="btn-cat" rel="nofollow"
                                   title="{!! $cat !!}">{!! ucwords($cat) !!}</a>
                            @endforeach
                        @endif
                    </p>
                </div>
            </div>
        </div>

    @empty
        <div class="col-md-12">
            <p>No result</p>
        </div>
    @endforelse
</div>
