@extends('template.layout')

@section('title', ucwords($keyword) . ' Contact Contact')
@section('description', ucwords($keyword) . '. Search phone number, email to contact service support '. ucwords($keyword).'. Find contact now!')

@section('contents')
    <div class="container" role="main" style="margin-top:40px">
        <h1 class="text-primary">{!! ucwords($keyword) !!} View Contact</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{!! ucwords($keyword) !!} View Contact</li>
        </ol>

        @include('template.social')

        <div class="row">
            @if(count($relatedKeywords) > 0)
                <div class="col-md-12">
                    <h2 class="pl-0 h3 list-group-heading text-info mb-0 mt-15">Related Searches</h2>
                    <div class="row related-search">
                        @foreach($relatedKeywords as $k => $item)
                            <div class="col-md-4 col-xs-12">
                                <p>
                                    <a href="{{route('keyword.index', getSlug(strip_tags($item)))}}"
                                       title="{!! $item !!}"> <span>›</span> {!! $item !!}
                                    </a>
                                </p>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
        <div class="row" style="margin-top:15px">
            <div class="col-md-8 col-xs-12">

                <div class="row">
                    @forelse($dataList as $key => $record)
                        @php
                            $numberValue = getSalesNumber($record['title']);
                            $random = \Carbon\Carbon::now()->subDays(rand(1, 15));
                            $categories = getCategories($record['description'], $record['title']);
                            $categories = empty($categories) ? [] : $categories;
                            $phones = getPhoneNumber($record['title'], $record['description']);
                            $pNumber = @$phones[0][0];
                            $emails = extractEmailsFrom($record['title'] . $record['description']);
                            if(@$record['link'] == 'bing.com/videos' || @$record['link'] == 'bing.com/images' || @$record['link'] == 'bing.com/news'){
                                continue;
                            }
                        @endphp

                        <div class="col-md-12 col-xs-12" id="{{$key}}">
                            <div class="hover-box mt-2 pt-2">
                                <div class="panel-body">
                                    @if(@$record['link'])
                                        <h3 class="cursor-pointer open-link text-primary"
                                            data-keyword="{{getSlug(strip_tags($record['title']))}}"
                                            data-url="{{route('outlink', ['website' => $record['link']])}}"  style="margin-top:0px">
                                            {!!  $record['title']  !!}</h3>
                                    @else
                                        <h3 class="cursor-pointer open-link text-primary" data-keyword="{{getSlug(strip_tags($record['title']))}}"  style="margin-top:0px">
                                            {!!  $record['title']  !!}</h3>
                                    @endif
                                    @if(@$record['ans']['items'])
                                        <p>{!! @$record['ans']['title'] !!}</p>
                                        @foreach($record['ans']['items'] as $itemAns)
                                            <div class="clear-both" style="margin-bottom: 5px;">
                                                @if(@$itemAns['img'])
                                                    <img src="{{$itemAns['img']}}" alt="{!! $itemAns['title'] !!}" loading="lazy" width="65px" style="float: left; margin-right: 5px;">
                                                @endif
                                                @if(@$itemAns['link'])
                                                    <a data-url="{{route('outlink', ['website' => $itemAns['link']])}}"
                                                       data-keyword="{{getSlug(strip_tags($itemAns['title']))}}"
                                                       class="open-link cursor-pointer">
                                                        <strong>{!! $itemAns['title'] !!}</strong>
                                                    </a>
                                                @endif
                                                <p style="margin-bottom: 2px">{!! $itemAns['desc'] !!}</p>
                                            </div>
                                        @endforeach
                                    @else
                                        <p>
                                            @if($pNumber && strlen(\Illuminate\Support\Str::slug($pNumber)) > 7 || @$emails[0])
                                                <button class="button-76 btn-green pull-left" style="margin-right:10px">
                                                    @if($pNumber && strlen(\Illuminate\Support\Str::slug($pNumber)) > 7)
                                                        <a class="phone_number" href="tel:{{$pNumber}}"
                                                           style="color: inherit; font-size: 20px;"><i class="fa fa-phone"></i> {{ $pNumber }}</a>
                                                        <br>
                                                    @endif
                                                    @if(@$emails[0])
                                                        <a class="email" href="mailto:{{@$emails[0]}}"
                                                           style="font-size: 14px"><i class="fa fa-envelope-o"></i> {{ @$emails[0]}}</a>
                                                    @endif
                                                </button>
                                            @endif
                                            <span class="badge badge-warning"><em>{{$random->diffForHumans()}}</em></span>
                                            {!! getBoldStringHtml($record['description'], [@$keyword, @$emails[0], $pNumber, 'contact' , 'us']) !!}
                                        </p>
                                        <p>
                                            @if(@$record['info'])
                                                @foreach($record['info'] as $info)
                                                    @php
                                                        $info2 = explode(':', $info);
                                                    @endphp
                                                    @if(@$info2[1])
                                                        <strong>› {!! @$info2[0] !!}:</strong> {!! @$info2[1] !!} <br>
                                                    @else
                                                        <span>› {!! $info !!}</span> <br>
                                                    @endif
                                                @endforeach
                                            @endif
                                            @if(@$record['more'])
                                                @foreach($record['more'] as $l => $more)
                                                    <span>{{$l+1}}. {!! @$more !!}</span> <br>
                                                @endforeach
                                            @endif
                                        </p>
                                        <hr>
                                        <p>
                                            <span class="pull-right">
                                                @if($pNumber && strlen(\Illuminate\Support\Str::slug($pNumber)) > 7)
                                                    <button class="btn btn-danger btn-sm btn-verify-phone" data-toggle="modal" data-target="#modalVerify">
                                                        <i class="fa fa-phone"></i> Verify phone</button>
                                                @endif
                                                @if(@$emails[0])
                                                    <button class="btn btn-warning btn-sm btn-verify-email" data-toggle="modal" data-target="#modalVerify">
                                                        <i class="fa fa-envelope"></i> Find more email</button>
                                                @endif
                                                <a href="#" data-toggle="modal" data-target="#modalPreview" class="btn btn-sm btn-warning btn-preview" style="cursor: pointer"
                                                   data-title="{{\Illuminate\Support\Str::slug($record['title'], ' ')}}"
                                                   data-domain="{{getDomain($record['link'])}}"
                                                ><i class="fa fa-eye"></i> Preview site</a>
                                                @if(@$record['link'] && getDomain(@$record['link']))
                                                    <a data-keyword="{{getSlug(strip_tags($record['title']))}}"
                                                       data-url="{{route('contact.website', getDomain($record['link']))}}"
                                                       class="btn btn-sm btn-primary open-link">Show detail
                                                        <span class="fa fa-caret-right"></span>
                                                    </a>
                                                @endif
                                            </span>
                                        </p>
                                        <p>
                                            @if(count($categories) > 0)
                                                @foreach($categories as $k => $cat)
                                                    <a data-keyword="{{getSlug($cat)}}" class="btn-cat cursor-pointer box-type open-link"
                                                       title="{!! $cat !!}">{!! ucwords($cat) !!}</a>
                                                @endforeach
                                            @endif

                                            <span class="text-primary"><i class="fa fa-users"></i> {{$record['people_number']}} contacted</span>
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                    @empty
                        <p style="padding-left: 7x;">No result</p>
                    @endforelse
                </div>
            </div>
            <div class="col-md-4 col-xs-12">

                @include('template.sidebar_right')

            </div>
        </div>
    </div>
@stop
