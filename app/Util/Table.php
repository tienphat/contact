<?php

namespace App\Util;

use DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Table
{
    public static function createTable($tableSlug)
    {
        $table = 'companies_'. $tableSlug;
        $sql = "
            CREATE TABLE IF NOT EXISTS `$table` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `category_id` int(11) DEFAULT NULL,
              `city_id` int(11) DEFAULT NULL,
              `id_craw` varchar(100) DEFAULT NULL,
              `slug` varchar(500) DEFAULT NULL,
              `name` varchar(500) DEFAULT NULL,
              `address` varchar(500) DEFAULT NULL,
              `phone` varchar(45) DEFAULT NULL,
              `fax` varchar(45) DEFAULT NULL,
              `website` varchar(500) DEFAULT NULL,
              `description` text,
              `year_established` varchar(45) DEFAULT NULL,
              `total_sale` varchar(45) DEFAULT NULL,
              `number_employees` varchar(45) DEFAULT NULL,
              `logo` varchar(500) DEFAULT NULL,
              `emails` varchar(500) DEFAULT NULL,
              `short_name` varchar(300) DEFAULT NULL,
              `score` float DEFAULT NULL,
              `mission` text,
              `ceo_name` varchar(191) DEFAULT NULL,
              `ceo_avatar` text,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";
        \Illuminate\Support\Facades\DB::statement($sql);
    }

    public static function setIndexTable($tableName)
    {
        $sql = "CREATE INDEX idx_name ON `$tableName` (name);";
        $sql1 = "CREATE INDEX idx_slug ON `$tableName` (slug);";
        $sql2 = "CREATE INDEX idx_id_craw ON `$tableName` (id_craw);";
        $sql3 = "CREATE INDEX idx_city_id ON `$tableName` (city_id);";
        $sql4 = "CREATE INDEX idx_category_id ON `$tableName` (category_id);";

        \Illuminate\Support\Facades\DB::statement($sql);
        \Illuminate\Support\Facades\DB::statement($sql1);
        \Illuminate\Support\Facades\DB::statement($sql2);
        \Illuminate\Support\Facades\DB::statement($sql3);
        \Illuminate\Support\Facades\DB::statement($sql4);
    }

    /**
     * To delete the tabel from the database
     *
     * @param $table_name
     *
     * @return bool
     */
    public static function removeTable($table_name)
    {
        try {
            Schema::dropIfExists($table_name);

            return true;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

}
