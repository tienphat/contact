<?php

set_time_limit(0);

use App\Models\Images;
use App\Models\Videos;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use LayerShifter\TLDExtract\Extract;

if (!function_exists('updateRecentlyKeyword')) {
    function updateRecentlyKeyword($key)
    {
        $keywords = Cache::get('recently-keyword-contact') ?? [];
        if(count($keywords) > 0){
            if(!in_array($key, $keywords)
                && substr_count($key, 'contact') <= 2
                && stringNeedInArray($key)
            ){
                $keywords[] = $key;

                if(count($keywords) > 30){
                    array_shift($keywords);
                }
            }
        } else {
            $keywords[] = $key;
        }
        Cache::put("recently-keyword-contact", $keywords, now()->addMinutes(30));

//        if ($keyW = Keywords::where(['keyword' => $key])->first()) {
//            $keyW->count = $keyW->count + 1;
//            $keyW->save();
//        } else {
//            $key = Str::slug(strtolower($key), ' ');
//            Keywords::insert(['keyword' => $key, 'count' => 1]);
//        }

        return $keywords;
    }
}

if (!function_exists('updateListImages')) {
    function updateListImages($key, $img)
    {
        if ($key != null && strlen($key) > 0 && $img && stringNeedInArray($key)) {
            if (!Images::where(['keyword' => checkCat($key)])->exists()) {
                Images::insert([
                    'keyword' => checkCat($key),
                    'img_url' => $img ,
                    'character' => strtolower(substr($key, 0, 1))
                ]);
            }
        }
    }
}

if (!function_exists('updateListVideos')) {
    function updateListVideos($key, $vid)
    {
        if ($key != null && strlen($key) > 0 && $vid && stringNeedInArray($key)) {
            if (!Videos::where(['keyword' => checkCat($key)])->exists()) {
                Videos::insert([
                    'keyword' => checkCat($key),
                    'video_url' => $vid ,
                    'character' => strtolower(substr($key, 0, 1))
                ]);
            }
        }
//        return $keywords;
    }
}

if (!function_exists('stringNeedInArray')) {
    function stringNeedInArray($string)
    {
        try {
            $arrNeed = [
                'contact',
                'phonenumber',
                'email',
                'call',
                'support',
            ];
            foreach ($arrNeed as $i) {
                if (strpos($string, strtolower($i)) !== false) {
                    return true;
                }
            }
            return false;
        } catch (Exception $e){
            return false;
        }
    }
}

if (!function_exists('extractEmailsFrom')) {
    function extractEmailsFrom($string)
    {
        preg_match_all(
            "/[a-z\d._%+-]+@\<em\>[a-z\d.-]+\.[a-z]{2,4}\b\<\/em\>/i",
            $string,
            $matches1
        );
        preg_match_all(
            "/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b/i",
            $string,
            $matches2
        );
        preg_match_all(
            "/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}+\.[a-z]{2,4}\b/i",
            $string,
            $matches3
        );
        $matches = array_merge($matches1, $matches2, $matches3);
        $emails = $arrCheck = [];
        foreach ($matches as $list) {
            if (empty($list)) {
                continue;
            }
            foreach ($list as $email) {
                $email = strtolower($email);
                $email = str_replace(['<em>', '</em>'], '', $email);
                if (isset($arrCheck[$email])) {
                    continue;
                }
                $arrCheck[$email] = 1;
                $emails[] = $email;
            }
        }

        return $emails;
    }
}

if (!function_exists('getPreEmail')) {
    function getPreEmail($string)
    {
        return explode('@', $string);
    }
}

if (!function_exists('isEmail')) {
    function isEmail($string)
    {
        if (filter_var($string, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('getPhoneNumber')) {
    function getPhoneNumber($title, $desc)
    {
        try {
            $string = $title . $desc;
            preg_match("/(\d+|\-|\+|\(|\)|\ ){0,}(\d+|\ |\-){8,14}/", $string, $phone[]);
//            preg_match_all('/[0-9]{3,4}[\-][0-9]{6}|[0-9]{3}[\s][0-9]{6}|[0-9]{3}[\s][0-9]{3}[\s][0-9]{4}|[0-9]{9}|[0-9]{3}[\-][0-9]{3}[\-][0-9]{4}/', $string, $phone);
//            preg_match_all('/\b[0-9]{3,4}\s*[-]?\s*[0-9]{3}\s*[-]?\s*[0-9]{4}\b/',$string, $phone);
            return $phone;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

if (!function_exists('stringInArray')) {
    function stringInArray($string)
    {
        $arrRemove = [
            'secret',
            'nude',
            'fuck',
            'girl',
            'sex',
            'star',
            'snapcam',
            'leak'
        ];
        foreach ($arrRemove as $item) {
            if (strpos(strtolower($string), $item) !== false){
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('checkCat')) {
    function checkCat($string)
    {
        if (!stringNeedInArray($string)){
            return $string . '-recipes';
        }

        return $string;
    }
}

if (!function_exists('getFileExcept')) {
    function getFileExcept()
    {
        //1800 = 30'
        $data = Cache::remember("cache_except", 1800, function () {
            return file(public_path('except.txt'), FILE_IGNORE_NEW_LINES);
        });
        return $data;
    }
}

if (!function_exists('getFileExceptSite')) {
    function getFileExceptSite()
    {
        //1800 = 30'
        $data = Cache::remember("cache_except_site", 1800, function () {
            return file(public_path('except_site.txt'), FILE_IGNORE_NEW_LINES);
        });
        return $data;
    }
}

if (!function_exists('getIDImagesBing')) {
    function getIDImagesBing($url)
    {
        $urlParse = parse_url($url);
        $exp = explode('&', @$urlParse['query']);
        foreach($exp as $item){
            $ex = explode('=', $item);
            if(@$ex[0] == 'id'){
                return @$ex[1];
            }
        }
        return @$exp['query'];
    }
}


if (!function_exists('getAllCategories')) {
    function getAllCategories()
    {
        $categories = config('categories');
        $data = [];
        foreach($categories as $item){
            $data[] = [
                'slug' => getSlug($item) . '-contact',
                'name' => $item
            ];
        }
        return $data;
    }
}

if (!function_exists('getMoreWebsiteText')) {
    function getMoreWebsiteText($domain)
    {
        $arr = [
            'See more all of contact on www.' . $domain,
            'Search Contact From www.' . $domain,
            'Discover The Contact www.' . $domain,
            'On roundup of the contact on www.' . $domain,
            'Contact From www.' . $domain,
            'Most Popular Contact Newest at www.' . $domain,
            'Search The Contact at www.' . $domain,
            'Contact the day at www.' . $domain,
            'Top Contact From www.' . $domain
        ];

        return $arr[rand(0, count($arr) - 1)];
    }
}

if (!function_exists('getEmbedYoutube')) {
    function getEmbedYoutube($url)
    {
        return str_replace('watch?v=', 'embed/', $url);
    }
}

if (!function_exists('getCategories')) {
    function getCategories($string, $title = null)
    {
        $categoriesList = getAllCategories();
        $catResp = [];
        foreach($categoriesList as $item){
            $catName = strtolower($item['name']);
            if (strpos($string, $catName) !== false) {
                $catResp[] = $catName;
            }
            if (@$title && strpos($title, $catName) !== false && !in_array($catName, $catResp)) {
                $catResp[] = $catName;
            }
        }

        return $catResp;
    }
}

if (!function_exists('getFlagOfCountry')) {
    function getFlagOfCountry($country = null)
    {
        if (!isset($country)) {
            return '';
        }

        $urlFlag = env('URL_IMAGE_COUNTRY');
        return $urlFlag = str_replace('{COUNTRY}', $country, $urlFlag);
    }
}

if (!function_exists('getVerifiedDate')) {
    function getVerifiedDate($time)
    {
        try{
            $time = explode(',', $time);
            $time = trim(str_replace('GMT', '', @$time[1]));
            $time = str_replace('thg', '', $time);
            $time = Carbon::createFromFormat('d m Y H:i:s', $time)->diffForHumans();
            return $time;
        } catch (Exception $e){
            return now()->subDay(rand(1, 5))->diffForHumans();
        }
    }
}

if (!function_exists('getBoldStringHtml')) {
    function getBoldStringHtml($string, $searchArr = [])
    {
        try{
//            foreach($searchArr as $search){
//                $explode = explode(' ', $search);
//                foreach($explode as $item){
//                    if (strlen($item) <= 2) {
//                        continue;
//                    }
//                    $k1 = strtoupper($item);
//                    $k2 = strtolower($item);
//                    $k3 = ucfirst($item);
//                    $text = "<strong>$item</strong>";
//                    $string = str_replace($item, $text, $string);
//                    $string = str_replace($k1, "<strong>$k1</strong>", $string);
//                    $string = str_replace($k2, "<strong>$k2</strong>", $string);
//                    $string = str_replace($k3, "<strong>$k3</strong>", $string);
//                }
//            }
            return $string;
        } catch (Exception $e){
            return now()->subDay(rand(1, 5))->diffForHumans();
        }
    }
}

if (!function_exists('getFlagOfWebsite')) {
    function getFlagOfWebsite($website)
    {
        $domain = getDomainOrigin($website);
        return "https://www.google.com/s2/favicons?domain=$domain";
    }
}

if (!function_exists('genDesc')) {
    function genDesc($companyInfo)
    {
        $description = $companyInfo->name . ' Company was founded in ' . $companyInfo->year_established . ' in ' . strip_tags($companyInfo->address) .
            ' by CEO ' . $companyInfo->ceo_name . '.'
            . ($companyInfo->number_employees ? ' Company size is about ' . $companyInfo->number_employees . ' employees' : '')
             . ($companyInfo->total_sale ? '. Total revenue is about ' . $companyInfo->total_sale : '') .'.';

        return $description;
    }
}

if (!function_exists('addHttp')) {
    function addHttp($url)
    {
        $parsed = parse_url($url);
        if (empty($parsed['scheme'])) {
            $url = 'http://' . ltrim($url, '/');
        }

        return $url;
    }
}
if (!function_exists('getDomainOrigin')) {
    function getDomainOrigin($domain)
    {
        $domain = trim($domain);

        $domain = str_replace(["https://", 'http://', 'http://www.', 'https://www.', 'www.'], '', $domain);
        $domain = strtolower(explode('/', $domain)[0]);

        return $domain;
    }
}
if (!function_exists('getDomainFromUrl')) {
    function getDomainFromUrl($url)
    {
        if (strlen($url) == 0 || is_null($url)) {
            return null;
        }
        $url = trim($url);
        $http = $www = '';
        if (strpos($url, 'http') !== false) {
            $http = 'http';
        } else if (strpos($url, 'https') !== false) {
            $http = 'https';
        } else {
            $http = 'https';
        }
        if (strpos($url, 'www') !== false) {
            $www = 'www';
        }

        $domain = getDomainOrigin($url);
        $domain = $http . '://' . ($www == '' ? '' : ($www . '.')) . $domain;

        return $domain;
    }
}

if (!function_exists('setCookieCustom')) {
    function setCookieCustom($name, $value, $minutes = 120)
    {
        setcookie($name, $value, $minutes);
    }
}

if (!function_exists('removeCookieCustom')) {
    function removeCookieCustom($name)
    {
        Cookie::queue(
            Cookie::forget($name)
        );
    }
}

if (!function_exists('getCookieCustom')) {
    function getCookieCustom($name)
    {
        return @$_COOKIE[$name];
    }
}

if (!function_exists('getAllCookie')) {
    function getAllCookie()
    {
        return $_COOKIE;
    }
}

if (!function_exists('truncateString')) {
    function truncateString($str, $width)
    {
        return strtok(wordwrap($str, $width, "...\n"), "\n");
    }
}

if (!function_exists('get_client_ip')) {
    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}

if (!function_exists('getFaviconWebsite')) {
    function getFaviconWebsite($domain)
    {
        if (empty($domain)) {
            return '';
        }

        return env('URL_GET_FAVICON_WEBSITE') . $domain;
    }
}

if (!function_exists('getDiffDate')) {
    function getDiffDate($date, $type = 'ago')
    {
        $datetime1 = new DateTime(now()->format('Y-m-d H:i:s'));
        $datetime2 = new DateTime(\Carbon\Carbon::parse($date)->format('Y-m-d H:i:s'));
        $interval = $datetime1->diff($datetime2);

        $format = '';
        if ($interval->format('%y') != 0) {
            $format .= '%y years ';
        }

        if ($interval->format('%m') != 0) {
            $format .= '%m months ';
        }

        if ($interval->format('%d') != 0) {
            $format .= '%d days ';
        }

        return $interval->format($format . ' ' . $type);
    }
}

if (!function_exists('isDomain')) {
    function isDomain($domain)
    {
        $extract = new Extract();
        $result = $extract->parse($domain);

        return $result->isValidDomain();
    }
}

if (!function_exists('getDiffHouse')) {
    function getDiffHouse($date)
    {
        $datetime1 = new DateTime(now()->format('Y-m-d H:i:s'));
        $datetime2 = new DateTime(\Carbon\Carbon::parse($date)->format('Y-m-d H:i:s'));
        $interval = $datetime1->diff($datetime2);

        $format = '';
        if ($interval->format('%y') != 0) {
            $format .= '%y years ';
        }

        if ($interval->format('%m') != 0) {
            $format .= '%m months ';
        }

        if ($interval->format('%d') != 0) {
            $format .= '%d days ';
        }

        if ($interval->format('%H') != 0) {
            $format .= '%H hours ';
        }

        if ($interval->format('%i') != 0) {
            $format .= '%i minutes ';
        }

        if ($interval->format('%s') != 0) {
            $format .= '%s seconds ';
        }

        if ($format == '') $format = '1 seconds';

        return $interval->format($format . ' ago');
    }
}

if (!function_exists('parseDateFormat')) {
    function parseDateFormat($date = null, $format = null)
    {
        if (empty($date)) {
            return now();
        }

        return !empty($format) ? Carbon::parse($date)->format($format) : Carbon::parse($date);
    }
}

if (!function_exists('addHttp')) {
    function addHttp($url)
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
}

if (!function_exists('addHttps')) {
    function addHttps($url)
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "https://" . $url;
        }
        return $url;
    }
}

if (!function_exists('addWww')) {
    function addWww($url)
    {
        if (!preg_match("~^www?~i", $url)) {
            $url = "www." . $url;
        }
        return $url;
    }
}

if (!function_exists('isDomain')) {
    function isDomain($domain)
    {
        $extract = new Extract();
        $result = $extract->parse($domain);

        return $result->isValidDomain();
    }
}

if (!function_exists('isIP')) {
    function isIP($ip)
    {
        if ($ip == '0.0.0.0') {
            return false;
        }

        $extract = new Extract();
        $result = $extract->parse($ip);

        return $result->isIp();
    }
}
//
//if (!function_exists('getSuffix')) {
//    function getSuffix($domain)
//    {
//        $extract = new Extract();
//        $result = $extract->parse($domain);
//
//        return $result->getSuffix();
//    }
//}

if (!function_exists('replaceTextDesc')) {
    function replaceTextDescList($string)
    {
        $string = str_replace('\n', '. ', $string);
        $string = str_replace('\r', '. ', $string);
        $string = str_replace('\t', ' ', $string);
        $string = str_replace('\"', '"', $string);
        return $string;
    }
}

if (!function_exists('getDescList')) {
    function getDescList($string)
    {
        $desc = truncateString(strip_tags($string), 205);
        return replaceTextDescList($desc);
    }
}

if (!function_exists('replaceTextDesc')) {
    function replaceTextDesc($string)
    {
        $string = str_replace('\r\n', '<br/>', $string);
        $string = str_replace('\u002F', '/', $string);
        $string = str_replace('\n', '<br/>', $string);
        $string = str_replace('\r', '', $string);
        $string = str_replace('\t', '   ', $string);
        return $string;
    }
}

if (!function_exists('removeTextBingInfo')) {
    function removeTextBingInfo($string)
    {
        $string = str_replace('New content will be added above the current area of focus upon selection', '', $string);
        $string = str_replace('Sẽ thêm nội dung mới vào phía trên khu vực tập trung hiện tại khi lựa chọn', '', $string);
        return $string;
    }
}

if (!function_exists('getNoImageUrl')) {
    function getNoImageUrl()
    {
        return asset('images/no-image.png');
    }
}

if (!function_exists('getNoAvatarUrl')) {
    function getNoAvatarUrl()
    {
        return asset('images/user-avatar.png');
    }
}

if (!function_exists('getExt')) {
    function getExt($domain)
    {
        $d = explode('.', $domain);
        $ext = strtolower(end($d));

        if (!preg_match("/^[_a-zA-Z0-9]+$/", $ext)) {
            return "";
        }

        return $ext;
    }
}

if (!function_exists('getDomain')) {
    function getDomain($domain)
    {
        $extract = new Extract();
        $result = $extract->parse($domain);

        return $result->getRegistrableDomain();
    }
}

if (!function_exists('subString')) {
    function subString($string, $length = 150)
    {
        $s = substr($string, 0, $length);
        return substr($s, 0, strrpos($s, ' '))  . '...';
//        return substr($string, 0, strpos($string, ' ', $length);
        return strlen($string) >= $length ? substr($string, 0, ($length - 3)) . '...' : $string;
    }
}

if (!function_exists('urlExists')) {
    function urlExists($url)
    {
        $file_headers = @get_headers($url);
        if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        } else {
            $exists = true;
        }
        return $exists;
    }
}

if (!function_exists('getCharacterAlphabet')) {
    function getCharacterAlphabet($merge = true)
    {
        $arrNumber = range(0, 9);
        $arrCharacter = range('a', 'z');
        if ($merge) {
            return array_merge($arrCharacter, $arrNumber);
        }
        return [
            'numbers' => $arrNumber,
            'characters' => $arrCharacter
        ];
    }
}


if (!function_exists('getFAQ')) {
    function getFAQ($questions = [])
    {
        if(!$questions){
            return '';
        }

        $faqJon = config('question_faq');
        $json = [];
        foreach ($questions as $k => $item) {
            $json[] = [
                '@type' => 'Question',
                'name' => $item['title'],
                'acceptedAnswer' => [
                    "@type" => "Answer",
                    "text" => $item['description']
                ]
            ];
        }

        $faqJon['mainEntity'] = $json;
        return json_encode($faqJon);
    }
}

if (!function_exists('curlBing')) {
    function curlBing($url)
    {
        try {
            $agents = array(
                'Sogou Pic Spider/3.0( http://www.sogou.com/docs/help/webmasters.htm#07)',
                'Sogou head spider/3.0( http://www.sogou.com/docs/help/webmasters.htm#07)',
                'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
                'Mediapartners-Google',
                'Sogou web spider/4.0(+http://www.sogou.com/docs/help/webmasters.htm#07)',
                'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERAGENT, $agents[array_rand($agents)]);
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
//            curl_setopt($ch, CURLOPT_REFERER, $ref);
            curl_setopt($ch, CURLOPT_COOKIEJAR, public_path('cookie.txt'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        } catch (Exception $e) {
            return '';
        }
    }
}

if (!function_exists('callAPICustom')) {
    function callAPICustom($url, $data = null, $method = 'GET')
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
//            CURLOPT_POSTFIELDS => $data ? json_encode($data) : null,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
//                "content-type: application/json",
//                "postman-token: 91d354f0-1caf-867a-d8fd-ca491092baff"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
            return [];
        } else {
            return $response;
        }
    }
}

if (!function_exists('parseQueryForLikeCond')) {
    function parseQueryForLikeCond($keySearch)
    {
        // replace multi space or space full-size into space half-size
        $keySearch = preg_replace("/(\s+)/us", " ", $keySearch);
        // replace 1 special \ into 2 special \ to search with LIKE in Mysql
        $keySearch = replaceQueryForLikeCond($keySearch);

        return explode(' ', $keySearch);
    }
}

if (!function_exists('replaceQueryForLikeCond')) {
    function replaceQueryForLikeCond($keySearch)
    {
        // replace 1 special \ into 2 special \ to search with LIKE in Mysql
        $keySearch = preg_replace('/\\\\/', '\\\\', $keySearch);
        // add special \ before special %,_
        $keySearch = preg_replace('/\%/', '\%', $keySearch);
        $keySearch = preg_replace('/\_/', '\_', $keySearch);

        return $keySearch;
    }
}


if (!function_exists('companiesTable')) {
    function companiesTable($countryName)
    {
        return Companies::PREFIX . Str::slug($countryName, '_');
    }
}

if(!function_exists('getSlug')){
    function getSlug($string, $partern = '-')
    {
        $string = Str::slug($string, $partern);
        $string = strtolower($string);
        $string = str_replace(['_','+', ' ', '|'], $partern, $string);

        if (strpos($string, '.') !== false) {
            return $string;
        }

        return Str::slug($string, $partern);
    }
}

if(!function_exists('removeDomainImg')){
    function removeDomainImg($stringText)
    {
        try{
            return encrypt(str_replace('https://www.tfrecipes.com/', '', $stringText));
        } catch (Exception $e){
            return null;
        }
    }
}

if(!function_exists('getSalesNumber')){
    function getSalesNumber($stringText)
    {
        try{
            preg_match_all('!\d+%!', $stringText, $matches);

            if(@$matches[0][0]){
                return @$matches[0][0];
            } else {
                preg_match_all('/^\$+\d+/i', $stringText, $matches2);
                return @$matches2[0][0];
            }

            return null;
        } catch (Exception $e){
            return null;
        }
    }
}

if (!function_exists('curl')) {
    function curl($url, $ref = '')
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_REFERER, $ref);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout in seconds
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
            $result = curl_exec($ch);
            curl_close($ch);

            if ($result === FALSE) {
                Log::info('Error CURL: ' . $url);
                return '';
            } else {
                return $result;
            }
        } catch (Exception $e) {
            $message = "CURL $url fail: " . $e->getMessage();
            echo $message . "<br/>";
//                Log::info($message);

//                sendEmail(['title' => "[CURL][$url] Email Notify", 'message' => $message]);
            return '';
        }
    }
}

