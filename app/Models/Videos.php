<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $table = 'videos';

    protected $fillable = [
        'keyword',
        'video_url',
        'character',
    ];

    public $timestamps = false;
}
