
@php
    $path = request()->path() == '/' ? null : request()->path();
    $pathLink = 'https://www.contactus.com/' . $path;
@endphp
<p>
    <a target="_blank"
       href="https://www.facebook.com/sharer.php?u={{$pathLink}}"
       title="Facebook Share">
        <img src="{{asset('images/fb.png')}}" width="25px"></a>
    <a target="_blank" href="https://plus.google.com/share?url={{$pathLink}}"
       title="Google Plus Share">
        <img src="{{asset('images/gp.png')}}" width="25px"></a>
    <a target="_blank" href="https://twitter.com/share?url={{$pathLink}}"
       title="Twitter Share">
        <img src="{{asset('images/tw.png')}}" width="25px"></a>
    <a target="_blank"
       href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{$pathLink}}"
       title="LinkedIn Share">
        <img src="{{asset('images/in.png')}}" width="25px"></a>
    <a target="_blank"
       href="https://pinterest.com/pin/create/button/?url={{$pathLink}}"
       title="Pinterest Share">
        <img src="{{asset('images/pin.png')}}" width="25px"></a>
    <a target="_blank"
       href="http://www.stumbleupon.com/badge/?url={{$pathLink}}"
       title="StumbleUpon Share">
        <img src="{{asset('images/su.png')}}" width="25px"></a>
    <a target="_blank" href="https://www.reddit.com/submit?url={{$pathLink}}"
       title="Reddit Share">
        <img src="{{asset('images/rt.png')}}" width="25px"></a>
    <a target="_blank"
       href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site {{$pathLink}}"
       title="E-Mail Share">
        <img src="{{asset('images/mail.png')}}" width="25px"></a>
</p>
