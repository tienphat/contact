<div class="row">
    <div class="col-md-12 text-center" style="margin-top: 20px">
        <h1 class="text-primary">Online Search Contact US</h1>
        <form action="{{route('search')}}" method="post">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-4 offset-md-3">
                    <input name="keyword" class="form-control" placeholder="Enter the keyword search now" type="text"
                           value="{{@$keyword}}">
                </div>
                <button class="col-md-2 btn btn-success" type="submit"><i class="fa fa-search"></i> Search</button>
            </div>
        </form>
    </div>
</div>
