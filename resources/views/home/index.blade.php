@extends('template.layout')

@section('contents')
    <div class="container" role="main" style="margin-top:40px">
        @include('template.search')
        <hr>

        <h2 class="text-primary mt-5">Related Searched</h2>
        <div class="row" style="margin-top: 20px;">
            @foreach($latestImages as $key => $item)
                @php
                    if($item->keyword == '' || strlen($item->keyword) == 0){
                        continue;
                    }
                    $title = ucwords(getSlug($item->keyword, ' '));
                @endphp
                <div class="col-md-2 col-4 box-recipes">
                    <a href="{{route('keyword.index', getSlug($item->keyword))}}" class="text-center" @if($key >= 10) rel="nofollow" @endif
                    title="{!! $title !!}">
                        <img src="{{$item->img_url}}" alt="{!! $title !!}" loading="lazy" width="100%">
                        {!! $title !!}
                    </a>
                </div>
            @endforeach
        </div>
        <hr>

        <h2 class="text-primary mt-5">Contacts Videos</h2>
        <div class="row">
            @foreach($latestVideos as $key => $item)
                @php
                    if($item->keyword == '' || strlen($item->keyword) == 0){
                        continue;
                    }
                    $title = ucwords(getSlug($item->keyword, ' '));
                @endphp
                <div class="col-md-3 col-6">
                    <video class="z-depth-1" loop controls style="width: 100%;">
                        <source src="{{$item->video_url}}" type="video/mp4" />
                    </video>
                    <a href="{{route('keyword.index', getSlug($item->keyword))}}" class="text-center" @if($key >= 10) rel="nofollow" @endif
                    title="{!! $title !!}">
                        {!! $title !!}
                    </a>
                </div>
            @endforeach
        </div>
        <hr>

        <h2 class="text-primary text-center">Top Categories</h2>

        <div class="row">
            @foreach($categoriesList as $item)
                <div class="col-md-2 col-xs-4">
                    <p><a href="{{route('keyword.index', $item['slug'])}}"
                          title="{{$item['name']}}">{{$item['name']}}</a>
                    </p>
                </div>
            @endforeach
        </div>
        <hr>

        @if(count(@$topFaqs) > 0)
            <h2 class="pl-0 text-info mb-0 mt-5 text-center">FAQs for Contact US?</h2>
            <div class="row faq">
                @foreach($topFaqs as $k => $item)
                    <div class="col-md-12 mt-2 item">
                        <h3>{!! $item['title'] !!}</h3>
                        <p class="pl-2">{!! $item['description'] !!}</p>
                    </div>
                @endforeach
            </div>
            <hr></hr>
        @endif

        @php
            $recentlyKeywords = \Illuminate\Support\Facades\Cache::get('recently-keyword-contact') ?? [];
        @endphp
        <h3 class="text-primary text-center">Recently Searched</h3>
        <div class="row" style="margin-bottom: 20px">
            @foreach($recentlyKeywords as $k => $item)
                <div class="col-md-4 col-xs-12" style="margin-top: 5px;">&nbsp;&#8250;
                    <a href="{{route('keyword.index', getSlug(strip_tags($item)))}}" @if($k > 3) rel="nofollow noopener noreferrer" @endif
                       title="{{ucfirst(getSlug(strip_tags($item), ' '))}}">{{ucfirst(getSlug(strip_tags($item), ' '))}}</a>
                </div>
            @endforeach
        </div>
    </div>
    <hr>
@stop
