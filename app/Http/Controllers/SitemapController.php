<?php

namespace App\Http\Controllers;

use App\Models\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;

class SitemapController extends Controller
{
    private $maxLink = 500;

//    public function index()
//    {
//        $countDomains = Cache::remember("imagesCache", 7200, function () {
//            return Images::count();
//        });
//        $numberSitemapFile = ceil($countDomains / $this->maxLink);
//
//        $numberSitemapFile = view('sitemap.index', compact('numberSitemapFile'));
//        return Response::make($numberSitemapFile)->header('Content-Type', 'text/xml;charset=utf-8');
//    }

    public function index(Request $request)
    {
        $data = Cache::remember("sitemap-index", 14400, function () {
            return Images::select(["id", "keyword"])
                ->inRandomOrder()
                ->limit($this->maxLink)
                ->get();
        });

        $content = view('sitemap.detail', compact('data'));
        return Response::make($content)->header('Content-Type', 'text/xml;charset=utf-8');
    }

    public function detail(Request $request)
    {
        $siteMapId = $request->id;
        $offset = ($siteMapId - 1) * $this->maxLink;
        $data = Cache::remember("cache-domains-detail-$siteMapId", 7200, function () use ($offset) {
            return Images::select(["id", "keyword"])
                ->orderBy("id", "asc")
                ->offset($offset)
                ->take($this->maxLink)
                ->get();
        });

        $content = view('sitemap.detail', compact('data', 'siteMapId'));
        return Response::make($content)->header('Content-Type', 'text/xml;charset=utf-8');
    }
}
