<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keywords extends Model
{
    protected $table = 'keywords';

    protected $fillable = [
        'keyword',
        'count',
        'title',
        'content',
    ];

    public $timestamps = false;
}
