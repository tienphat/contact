<div class="row">
    @forelse($dataList as $key => $record)
        @php
            $categories = getCategories($record['description'], $record['title']);
            $categories = empty($categories) ? [] : $categories;
            $phones = getPhoneNumber($record['title'], $record['description']);
            $phoneCheck = str_replace([' ', '-'], '', $phoneCheck);
            $phoneGet = str_replace([' ', '-'], '', @$phones[0][0]);
            if(!@$phones[0][0] || ($phoneCheck != $phoneGet)){
                continue;
            }
        @endphp

        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 class="text-primary" style="margin-top:0px;font-size: 18px;">
                        <a data-url="{{route('outlink', ['website' => $record['link']])}}" class="open-link cursor-pointer">{!!  $record['title']  !!}</a>
                    </h3>
                    <p>
                        @if(@$phones[0][0])
                            <button class="btn btn-warning pull-left" style="margin-right:10px">
                                <a class="phone_number" href="tel:{{ @$phones[0][0] }}" rel="nofollow noopener noreferrer"
                                   style="color: inherit; font-size: 20px;"><i class="fa fa-phone"></i> {{ @$phones[0][0] }}</a><br>
                            </button>
                        @endif
                        {!! getBoldStringHtml($record['description'], [@$keyword, @$phones[0][0]]) !!}
                        <a data-url="{{route('outlink', ['website' => $record['link']])}}" class="open-link cursor-pointer">
                            View Detail <span class="glyphicon glyphicon-new-window"></span>
                        </a>
                    </p>
                    <p>
                        @if(count($categories) > 0)
                            @foreach($categories as $k => $cat)
                                <a data-keyword="{{getSlug($cat)}}" class="btn-cat cursor-pointer box-type open-link" title="{!! $cat !!}">{!! ucwords($cat) !!}</a>
                            @endforeach
                        @endif
                    </p>
                </div>
            </div>
        </div>

    @empty
        <div class="col-md-12">
            <p>No result</p>
        </div>
    @endforelse
</div>
