<div class="row">
    @forelse($dataList as $key => $record)
        @if(@$record['turl'])
            <div class="col-md-3 col-xs-12" style="margin-bottom: 7px;cursor: pointer">
                <img src="{{route('image', getIDImagesBing(@$record['turl']))}}" class="img-cover" alt="{{@$record['t']}}" loading="lazy" width="100%">
                {{--                <img src="{{@$record['turl']}}" class="img-cover" alt="{{@$record['t']}}" loading="lazy" width="100%">--}}
            </div>
        @endif
    @empty
        <div class="col-md-12">
            <p>No result</p>
        </div>
    @endforelse
</div>
<div class="row">
    <h4 class="text-secondary">
        <strong>You are previewing information from
            <a data-url="http://www.{{$domain}}" rel="nofollow noopener noreferrer" class="open-link">www.{{$domain}}</a>
        </strong>
    </h4>
    <div class="btn btn-default" style="border:5px dotted #FFF;background-color:#CCC;font-weight:bold">
        www.{{$domain}}
    </div>
    <a data-url="http://www.{{$domain}}" rel="nofollow noopener noreferrer"  class="btn btn-outline-primary open-link">
        <strong>Go Now</strong>
    </a>
</div>
