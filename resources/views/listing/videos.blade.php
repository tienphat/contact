@extends('template.layout')

@php
    $page = $page ? ' | Page ' . $page : '';
@endphp
@section('title', 'Contacts Videos Library - ' . ucwords($keyword) . $page)

@section('contents')
    <div class="container" role="main" style="margin-top:70px">
        <h1 class="text-primary">Contacts Videos Library - {!! ucwords($keyword) !!}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Contacts Videos Library - {!! ucwords($keyword) !!}</li>
        </ol>

        @include('template.social')

        <div class="row" style="margin-top:15px">
            <div class="col-md-12 col-12"><strong>Search: </strong> &nbsp;
                <a href="{{route('videos.index', 'a')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>A</strong></a>&nbsp;
                <a href="{{route('videos.index', 'b')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>B</strong></a>&nbsp;
                <a href="{{route('videos.index', 'c')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>C</strong></a>&nbsp;
                <a href="{{route('videos.index', 'd')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>D</strong></a>&nbsp;
                <a href="{{route('videos.index', 'e')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>E</strong></a>&nbsp;
                <a href="{{route('videos.index', 'f')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>F</strong></a>&nbsp;
                <a href="{{route('videos.index', 'g')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>G</strong></a>&nbsp;
                <a href="{{route('videos.index', 'h')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>H</strong></a>&nbsp;
                <a href="{{route('videos.index', 'i')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>I</strong></a>&nbsp;
                <a href="{{route('videos.index', 'j')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>J</strong></a>&nbsp;
                <a href="{{route('videos.index', 'k')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>K</strong></a>&nbsp;
                <a href="{{route('videos.index', 'l')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>L</strong></a>&nbsp;
                <a href="{{route('videos.index', 'm')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>M</strong></a>&nbsp;
                <a href="{{route('videos.index', 'n')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>N</strong></a>&nbsp;
                <a href="{{route('videos.index', 'o')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>O</strong></a>&nbsp;
                <a href="{{route('videos.index', 'p')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>P</strong></a>&nbsp;
                <a href="{{route('videos.index', 'q')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>Q</strong></a>&nbsp;
                <a href="{{route('videos.index', 'r')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>R</strong></a>&nbsp;
                <a href="{{route('videos.index', 's')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>S</strong></a>&nbsp;
                <a href="{{route('videos.index', 't')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>T</strong></a>&nbsp;
                <a href="{{route('videos.index', 'u')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>U</strong></a>&nbsp;
                <a href="{{route('videos.index', 'v')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>V</strong></a>&nbsp;
                <a href="{{route('videos.index', 'w')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>W</strong></a>&nbsp;
                <a href="{{route('videos.index', 'x')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>X</strong></a>&nbsp;
                <a href="{{route('videos.index', 'y')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>Y</strong></a>&nbsp;
                <a href="{{route('videos.index', 'z')}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>Z</strong></a>&nbsp;

                @for($i = 1; $i<= 9; $i++)
                    <a href="{{route('videos.index', $i)}}" class="btn btn-outline-secondary btn-sm mb-2" style="border-radius:15px"><strong>{{$i}}</strong></a>&nbsp;
                @endfor
            </div>

            <div class="col-md-12 col-xs-12">
                <div class="row" style="margin-top: 20px;">
                    @foreach($videos as $item)
                        @php
                            $title = ucwords(getSlug($item->keyword, ' '));
                        @endphp
                        <div class="col-md-4 col-12">
                            <video class="z-depth-1" loop controls style="width: 100%;">
                                <source src="{{$item->video_url}}" type="video/mp4" />
                            </video>
                            <a href="{{route('keyword.index', getSlug($item->keyword))}}" class="text-center" style="margin-bottom: 10px"
                               title="{!! $title !!}">
                                {!! $title !!}
                            </a>
                        </div>
                    @endforeach
                </div>

                {{$videos->links()}}
            </div>
        </div>
        <hr>
    </div>
@stop

@push('script')

@endpush
