<?php

namespace App\Providers;

use App\Models\Keywords;
use App\Services\BrowserService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('*', function ($view) {
            $keyword = "contact us email phone";
            $browserService = new BrowserService();
            $dataBing = Cache::remember("top-keyword-contact", 1800, function () use ($browserService, $keyword) {
                return $browserService->bingSearchNormal($keyword);
            });

            $topKeywords = @$dataBing[1] ?? [];
            $topFaqs = @$dataBing[2] ?? [];
            $view->with('topKeywords', $topKeywords);
            $view->with('topFaqs', $topFaqs);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(env('APP_ENV') != 'local'){
            $this->app['request']->server->set('HTTPS','on');
        }
    }
}
