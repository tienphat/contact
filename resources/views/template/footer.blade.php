<div class="modal fade" id="modalVerify" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title text-primary" style="line-height: 35px;text-transform: capitalize;font-size: 25px;">Verify This Contact: <span class="title_phone"></span> <span
                        class="title_email"></span></h2>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="content_before">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img class="" src="{{asset('images/loading.gif')}}" alt="Loading" width="50px">
                        </div>
                    </div>
                </div>
                <div class="content">
                </div>
                <hr>
                <h4 class="text-primary mt-3">Sign up to enter the latest information</h4>
                <h4>
                    <form action="" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <input type="email" class="form-control email-register" name="email"
                                       placeholder="Type your email address" required="">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-register-mail" type="submit">Register</button>
                                <button class="btn btn-danger" type="reset">Cancel</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <p>Was this information useful to you?</p>
                </h4>
                <h1 class="text-center">
                    <a href="#" rel="nofollow noopener noreferrer" class="btn-upvote"><i
                            class="fa fa-hand-o-up text-primary"></i></a> &nbsp;
                    <a href="#" rel="nofollow noopener noreferrer" class="btn-downvote"><i
                            class="fa fa-hand-o-down text-danger"></i></a>
                </h1>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPreview" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title text-primary" style="line-height: 35px;text-transform: capitalize;font-size: 25px;"></h2>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="content_before">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img class="" src="{{asset('images/loading.gif')}}" alt="Loading" width="50px">
                            </div>
                        </div>
                    </div>
                    <div class="content">

                    </div>
                </div>
                <hr>
                <h5 class="text-primary mt-3">Get the latest notifications from our website</h5>
                <form action="" method="POST">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-6">
                            <input type="email" class="form-control email-register" name="email"
                                   placeholder="Type your email address" required="">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-register-mail" type="submit">Register</button>
                            <button class="btn btn-danger" type="reset">Cancel</button>
                        </div>
                    </div>
                </form>
                <hr>
                <p>Did this information work for you?</p>
                <h2 class="text-center">
                    <a href="#" rel="nofollow noopener noreferrer" class="btn-upvote"><i
                            class="fa fa-hand-o-up text-primary"></i></a> &nbsp;
                    <a href="#" rel="nofollow noopener noreferrer" class="btn-downvote"><i
                            class="fa fa-hand-o-down text-danger"></i></a>
                </h2>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <footer>
        <div class="row">
            <div class="col-md-12 col-xs-12 text-center box-alphabet">
{{--                @include('template.alphabet')--}}

                <h3 class="text-primary text-left">About US</h3>
                <p class="text-justify">The display of third-party trademarks and trade names on this site does not
                    necessarily indicate any affiliation or endorsement of SiteContact.Org.</p>
                <p class="text-justify">If you click a merchant link and buy a product or service on their website, we may
                    be paid a fee by the merchant.</p>
            </div>
        </div>
        <hr>
        <p><strong>&copy; 2020 SiteContact.Org. All rights reserved</strong></p>
    </footer>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(".btn-verify-phone").on("click", function () {
        let e = $(this).parents(".panel-body").find(".phone_number").text(), t = $("#modalVerify");
        t.find(".title_phone").text(e), t.find(".title_email").text(""), $(".content_before").show(), $(".content").hide(), $.ajax({
            url: "{{route('verify')}}",
            data: {keyword: e}
        }).done(function (e) {
            $(".content_before").hide(), $(".content").show(), t.find(".content").html(e)
        })
    }), $(".btn-preview").on("click", function () {
        let d = $(this).data("domain"),
            t = $(this).data('title'),
            n = $("#modalPreview");
        n.find(".modal-title").text(t),
            $(".content_before").show(),
            $(".content").hide();
        $.ajax({
            url: "{{route('preview')}}",
            data: {domain: d, title: t}
        }).done(function (e) {
            $(".content_before").hide(), $(".content").show(), n.find(".content").html(e)
        })
    }), $(".btn-verify-email").on("click", function () {
        let e = $(this).parents(".panel-body").find(".email").text(), t = $("#modalVerify");
        t.find(".title_email").text(e), t.find(".title_phone").text(""), $(".content_before").show(), $(".content").hide(), $.ajax({
            url: "{{route('verify')}}",
            data: {keyword: e}
        }).done(function (e) {
            $(".content_before").hide(), $(".content").show(), t.find(".content").html(e)
        })
    }), $(".btn-register-mail").on("click", function (e) {
        e.preventDefault();
        let t = $(".email-register").val();
        t ? /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(t) ? alert("Thanks " + t + "! We will send you the latest information.") : ($(".email-register").focus(), alert("Please enter valid email!")) : ($(".email-register").focus(), alert("Please enter your email!"))
    }), $(".btn-upvote").on("click", function () {
        alert("Upvote successful!")
    }), $(".btn-downvote").on("click", function () {
        alert("Downvote successful!")
    }), $('body').on('click', ".open-link", function () {
        var dataId = $(this).attr("data-keyword");
        var dataUrl = $(this).attr("data-url");
        let url = '';
        let domain = '{{env('APP_URL')}}'
        if (dataId) {
            url = domain + '/contact/' + dataId;
        } else {
            url = dataUrl;
        }
        if (dataUrl && dataId) {
            window.open(dataUrl, "_blank");

            location.href = domain + '/contact/' + dataId;
        } else {
            location.href = url;
        }
    });;
</script>
</html>
