<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Redirect Link www.{{getDomain($website)}}</title>
    <meta name="description" content="Redirect Link www.{{getDomain($website)}}"/>
    <style>
        .container {
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto
        }

        @media (min-width: 1200px) {
            .container {
                width: 1170px;
            }
        }

        @media (min-width: 768px) {
            .container {
                width: 748px
            }
        }

        @media (min-width: 992px) {
            .container {
                width: 972px
            }
        }

        @media (min-width: 992px) {
            .container {
                width: 972px
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div style="text-align: center; margin-top: 50px;">
            <h1>Redirecting to <span style="color: #2e6da4">{{$website}}</span></h1>
            <h3>If not redirect, please click <a href="{{$website}}" rel="nofollow noopener noreferrer">here!</a></h3>
            @php
                header( "refresh:3;url=$website" );
                exit();
            @endphp
        </div>
    </div>
</body>
</html>
