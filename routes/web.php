<?php

Route::get('/', 'HomeController@index')->name('home');

Route::post('/search/{keyword?}', 'HomeController@search')->name('search');
Route::get('/contact/{keyword}', 'HomeController@detailList')->name('keyword.index');
Route::get('/listing-images/{keyword}/{page?}', 'HomeController@listingImages')->name('images.index');
Route::get('/listing-videos/{keyword}/{page?}', 'HomeController@listingVideos')->name('videos.index');
Route::get('/site/{website}', 'HomeController@detailSite')->name('contact.website');
Route::get('/out-link', 'HomeController@outLink')->name('outlink');
//Route::get('/categories/{cat}', 'HomeController@categories')->name('categories');


Route::get('/verify-phone', 'HomeController@verify')->name('verify');
Route::get('/site', 'HomeController@preview')->name('preview');
Route::get('/img/{id}', 'HomeController@image')->name('image');

Route::post('/comments', 'HomeController@comment')->name('comment');

Route::get('/404.html', 'HomeController@notFound')->name('404');
Route::get('/500.html', 'HomeController@serverError')->name('500');


Route::get('sitemap.xml', 'SitemapController@index')->name('sitemap');
