<style>
    .filter_search .panel-body {
        border: 1px solid #dddddd;
        border-radius: 5px;
    }
</style>
<div class="row filter_search">
    <div class="col-md-12">
        <div class="panel" style="padding-top: 20px; padding-bottom: 20px">
            <div class="panel-body pt-2 pb-2 pl-2 pr-2" >
                <form method="post" action="{{route('search')}}">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" value="{{$keyword}}" placeholder="Enter the keyword search apps"
                                   name="keyword" class="form-control border-radius-none">
                        </div>
                        <div class="col-md-12 pt-15 text-center">
                            <strong>Filter by:</strong>
                            <input type="radio" name="search_type" id="all" value=""
                                   @if(!$searchType )checked @endif>
                            <label for="all">All</label>
                            <input type="radio" name="search_type" id="phone_number" value="phone_number"
                                   @if($searchType == 'phone_number') checked @endif>
                            <label for="free">Phone Number</label>
                            <input type="radio" name="search_type" id="mobile_phone" value="mobile_phone"
                                   @if($searchType == 'mobile_phone') checked @endif>
                            <label for="top-downloads">Mobile phone</label>
                            <input type="radio" name="search_type" id="contact_us" value="contact_us"
                                   @if($searchType == 'contact_us') checked @endif>
                            <label for="top-paid">Contact US</label>
                            <input type="radio" name="search_type" id="customer_service" value="customer_service"
                                   @if($searchType == 'customer_service') checked @endif>
                            <label for="top-paid">Customer Service</label>
                        </div>
                        <div class="col-md-4 offset-md-4">
                            <button class="btn btn-success border-radius-none w-100" type="submit"><span
                                        class="fa fa-search"></span> Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
