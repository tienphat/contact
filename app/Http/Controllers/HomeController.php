<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Comments;
use App\Models\Images;
use App\Models\Keywords;
use App\Models\Videos;
use App\Services\BrowserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    protected $browserService;

    public function __construct()
    {
        $this->browserService = new BrowserService();
    }

    public function index(Request $request)
    {
        try {
            $categoriesList = Cache::remember("categories-list-contact", 7200, function () {
                return getAllCategories();
            });

            if ($request->clear == true || $request->clear) {
                Artisan::call('cache:clear');
            }

            $latestImages = Images::orderBy('id', 'desc')
                ->limit(12)
                ->get();

            $latestVideos = Videos::orderBy('id', 'desc')
                ->limit(8)
                ->get();

            return view('home.index', compact( 'categoriesList', 'latestImages', 'latestVideos'));
        } catch (\Exception $e) {
            return redirect()->route('home')->with($e->getMessage());
        }
    }

    public function search(Request $request)
    {
        try {
            $keyword = getSlug($request->keyword);
            $searchType = $request->search_type;
            $type = $request->type;
            if (!@$keyword) {
                throw new \Exception('Not found!');
            }

            if ($type == 'website' || isDomain($request->keyword)) {
                $url = route('contact.website', ['website' => $request->keyword, 'search_type' => $searchType]);
            } else {
                if(!stringNeedInArray($keyword)){
                    $keyword .= '-contact';
                }
                $url = route('keyword.index', ['keyword' => $keyword, 'search_type' => $searchType]);
            }
            header('Location: ' . $url);
            exit();
        } catch (\Exception $e){
            return redirect()->route('home')->with($e->getMessage());
        }
    }

    public function detailList(Request $request)
    {
        try {
            $keywordOrg = $request->keyword;

            if (isDomain($keywordOrg)) {
                $url = route('contact.website', ['website' => $request->keyword]);
                header('Location: ' . $url);
                exit();
            }
            $keyword = str_replace(['-', '_'], ' ', $request->keyword);
            $searchType = $request->search_type;
            $arrExcept = getFileExcept();
            if (!@$keyword || in_array(@$keyword, $arrExcept) || stringInArray($keyword)) {
                return Redirect::to(route('home'), 301);
            }

            if (!stringNeedInArray($keyword)) {
                throw new \Exception('Not found', 404);
            }
            if ($request->remove || $request->remove == true) {
                file_put_contents(public_path('except.txt'), $keyword . PHP_EOL, FILE_APPEND | LOCK_EX);
                Artisan::call('cache:clear');
                return redirect()->route('home')->with('Removed: ' . $keyword);
            }
            $expKeyword = explode(' ', $keyword);
            $keywordSearch = $keyword;
            if (!@$expKeyword[1] || strpos($keyword, 'contact') === false) {
                $keywordSearch = $keyword . ' contact us';
            }

            //Insert keyword
            updateRecentlyKeyword($keyword);

            $keywordSearch .= Str::slug($searchType, ' ');
            $keywordSearchFake = $keywordSearch;

            $page = $request->page ?? 0;
            $dataBing = Cache::remember("bing-data-contact-$keyword-$searchType-$page", 1800, function () use ($keywordSearchFake, $page) {
                $data = $this->browserService->bingSearchNormal($keywordSearchFake, 45, $page);
                foreach ($data as $k => $item) {
                    if ($k == 0) {
                        foreach ($item as $key => $value) {
                            $item[$key]['people_number'] = rand(10, 100);
                        }
                        $data[0] = $item;
                    }
                }
                return $data;
            });
            $dataList = @$dataBing[0] ?? [];
            $relatedKeywords = @$dataBing[1] ?? [];
            $questions = @$dataBing[2] ?? [];
            $commonInfo = @$dataBing[3] ?? [];
            $listPhones = @$dataBing[4] ?? [];
            $videos = @$dataBing[5] ?? [];

            $comments = Comments::select('comments.*')->join('keywords', 'keywords.id', 'comments.keyword_id')
                ->where('keyword', $keyword)
                ->orderBy('comments.id', 'desc')
                ->get();

            $keywordOrg = Str::slug(strtolower($keyword), ' ');
            $keyword = $keywordSearch;

            return view('keyword.list', compact('dataList', ['keyword', 'keywordOrg', 'page', 'searchType', 'relatedKeywords', 'questions', 'listPhones', 'comments', 'videos']));
        } catch (\Exception $e){
            abort($e->getCode());
            return redirect()->route('home')->with($e->getMessage());
        }
    }

    public function detailSite(Request $request)
    {
        try {
            $keyword = $request->website;
            if (!isDomain($keyword)) {
                throw new \Exception('Not found!');
            }

            $arrExcept = getFileExceptSite();
            if(!@$keyword || in_array(@$keyword, $arrExcept)){
                return Redirect::to(route('home'), 301);
            }
            if ($request->remove || $request->remove == true) {
                file_put_contents(public_path('except_site.txt'), $keyword . PHP_EOL, FILE_APPEND | LOCK_EX);
                Artisan::call('cache:clear');
                return redirect()->route('home')->with('Removed: ' . $keyword);
            }
            $searchType = $request->search_type;
            $keywordSearch = 'site:'. $keyword;
            if ($searchType) {
                $keywordSearch .= ' ' . $searchType;
            }
            $page = $request->page ?? 0;

            $dataBing = Cache::remember("bing-contact-site-$keyword-$searchType-$page", 1800, function () use ($keywordSearch, $page) {
                $data = $this->browserService->bingSearchNormal($keywordSearch, 30, $page);
                foreach ($data as $k => $item) {
                    if ($k == 0) {
                        foreach ($item as $key => $value) {
                            $item[$key]['people_number'] = rand(3, 100);
                        }
                        $data[0] = $item;
                    }
                }
                return $data;
            });
            $dataList = @$dataBing[0] ?? [];
            $relatedKeywords = @$dataBing[1] ?? [];
            $questions = @$dataBing[2] ?? [];
            $commonInfo = @$dataBing[3] ?? [];
            $listPhones = @$dataBing[4] ?? [];
            $videos = @$dataBing[5] ?? [];

            ini_set('user_agent', 'Mozilla/5.0 (compatible; YandexMedia/3.0; +http://yandex.com/bots)');
            $websiteInfo = @get_meta_tags(addHttps(addWww($keyword)));

            return view('website.list', compact('dataList', ['keyword', 'page', 'searchType', 'relatedKeywords', 'websiteInfo', 'questions', 'videos', 'commonInfo', 'listPhones']));
        } catch (\Exception $e){
            return redirect()->route('home')->with($e->getMessage());
        }
    }

    public function outlink(Request $request)
    {
        try {
            $website = addHttps($request->website);
            if (!$website) {
                throw new \Exception('Not Found!');
            }
            return view('redirect', compact('website'));
        } catch (\Exception $e) {
            return redirect()->route('home')->with($e->getMessage());
        }
    }

    public function verify(Request $request)
    {
        try {
            $keyword = $request->keyword;
            if (isEmail($keyword)) {
                $emailInfo = getPreEmail($keyword);
                $preMail = $emailInfo[0];
                $afterMail = @$emailInfo[1];
                $keyword = '"*@' . $afterMail . '"';
            } else {
                $keyword = $keyword . ' contact phone';
                $phoneCheck = str_replace([' ', '-'],'', $request->keyword);
            }

            $dataBing = Cache::remember("bing-data-verify-$keyword", 1800, function () use ($keyword) {
                return $this->browserService->bingSearchNormal($keyword, 100);
            });
            $dataList = @$dataBing[0] ?? [];

            if(isEmail($request->keyword)){
                return view('template.email-site-info', compact('dataList', 'preMail', 'afterMail'));
            } else {
                return view('template.site-info', compact('dataList', 'phoneCheck'));
            }
        } catch (\Exception $e) {
            return redirect()->route('home')->with($e->getMessage());
        }
    }

    public function preview(Request $request)
    {
        try {
            $title = $request->title;
            $domain = $request->domain;
            $dataBing = Cache::remember("bing-data-site-$title", 1800, function () use ($title) {
                return $this->browserService->getImagesFromBing($title, 30);
            });
            $dataList = @$dataBing['images'] ?? [];

            return view('template.images-bing', compact('dataList', 'title', 'domain'));
        } catch (\Exception $e) {
            return redirect()->route('home')->with($e->getMessage());
        }
    }

    public function image(Request $request)
    {
        try {
            $id = $request->id;
            $url = 'https://www.bing.com/th?id=' . $id;
            return Redirect::to($url, 301);

        } catch (\Exception $e) {
            return '';
        }
    }

    public function comment(Request $request)
    {
        try {
            if(!$_POST['g-recaptcha-response']){
                Session::flash('missing_captcha', 'Please check you are not a robot!');
                throw new \Exception('Error');
            } else {
                //your site secret key
                $secret = '6Leb-AgdAAAAAAhnG7o9Yu8fkQuiL5EnzN6AgaWC';
                //get verify response data
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
                $responseData = json_decode($verifyResponse);
                if($responseData->success){
                    $succMsg = 'Your contact request have submitted successfully.';
                }else{
                    $errMsg = 'Robot verification failed, please try again.';
                    Session::flash('missing_captcha', $errMsg);
                    throw new \Exception('Error');
                }
            }

            $keyword = Keywords::where('keyword', $request->keyword)->first();
            if(!$keyword){
                throw new \Exception('Error');
            }
//            Comments::create([
//                'keyword_id' => $keyword->id,
//                'title' => $request->title,
//                'content' => $request->contents
//            ]);

            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back()->with($e->getMessage());
        }
    }

    public function notFound()
    {
        return view('errors.404');
    }

    public function serverError()
    {
        return view('errors.500');
    }


    public function listingImages(Request $request)
    {
        try {
            $keyword = $request->keyword;
            $images = Images::where('character', $keyword)
                ->orderBy('keyword', 'asc')
                ->simplePaginate(48);

            $page = $request->page ?? null;
            return view('listing.images', compact('images', 'keyword', 'page'));
        } catch (\Exception $e) {
            return redirect()->back()->with($e->getMessage());
        }
    }

    public function listingVideos(Request $request)
    {
        try {
            $keyword = $request->keyword;
            $videos = Videos::where('character', $keyword)
                ->orderBy('keyword', 'asc')
                ->simplePaginate(30);

            $page = $request->page ?? null;
            return view('listing.videos', compact('videos', 'keyword', 'page'));
        } catch (\Exception $e) {
            return redirect()->back()->with($e->getMessage());
        }
    }
}
