@if(@$bingInfo['common'] && @$bingInfo['dataSocials'])
    <div class="col-md-12">
        <div class="row">
{{--            <div class="panel panel-success">--}}
{{--                <div class="panel-body">--}}
                    @if(@$bingInfo['common'])
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="text-info intro_country">Introducing the country of {{@$countryInfo->name}}</h3>
                            </div>
                            <div class="col-md-3">
                                <img src="{{@$bingInfo['common']['icon']}}" alt="Icon" width="100%">
                            </div>
                            <div class="col-md-9">
                                {!! removeTextBingInfo($bingInfo['common']['description']) !!}

                                @if(@$bingInfo['dataSocials'])
                                    <div class="row pt-15">
                                        <div class="col-md-12">
                                            <ul class="list-inline list-unstyled">
                                                @foreach(@$bingInfo['dataSocials'] as $item)
                                                    <li>
                                                        <a href="{{$item['link']}}" target="_blank" rel="nofollow noopener noreferrer" class="text-info">
                                                            <img src="{{$item['img_src']}}" alt="{{$item['name']}}" width="30px" title="{!! $item['name'] !!}">
                                                            {!! $item['name'] !!}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif

                                @if(@$bingInfo['dataInfo'])
                                    <div class="row pt-15">
                                        <div class="col-md-12">
                                            <ul class="list-unstyled">
                                                @foreach(@$bingInfo['dataInfo'] as $key => $item)
                                                    <li class="mt-2"><strong>{!! $key !!}:</strong> {!! $item !!}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif
{{--                </div>--}}
{{--            </div>--}}
            <hr>
        </div>
    </div>
@endif
