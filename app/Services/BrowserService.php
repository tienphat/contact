<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use KubAT\PhpSimple\HtmlDomParser;
use Exception;

class BrowserService
{
    private $cmd = 'google-chrome-stable --no-sandbox --headless --disable-gpu --dump-dom ';

    public function bingSearchNormal($keyword, $limit = 40, $page = 0)
    {
        try {
            $urlOrigin = env('URL_BING_SEARCH_NORMAL');
            if (isDomain($keyword)) {
                $keyword = $keyword;
            } elseif (strpos($keyword, ':') !== false) {
                $keyword = $keyword;
            } else {
                $keyword = Str::slug($keyword, '+');
            }
            $offset = $page != 0 ? $limit * ($page - 1) : 0;
            $url = str_replace('{{KEYWORD}}', $keyword, $urlOrigin);
            $url = str_replace('{{LIMIT}}', $limit, $url);
            $url = str_replace('{{OFFSET}}', $offset, $url);

            $html = curlBing($url);
            $htmlDOMPage = HtmlDomParser::str_get_html($html);
            if (empty($htmlDOMPage) || empty($htmlDOMPage->find(".b_algo"))) {
                return [];
            }
            return self::getResultFromHtmlBing($htmlDOMPage);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function getResultFromHtmlBing($htmlDOMPage)
    {
        try {
            $arrData = $keywordRelated = $questions = $phones = [];
            foreach ($htmlDOMPage->find(".b_algo,.b_ans") as $key => $site) {
                if (!@$site->find('h2 a')[0]) {
                    continue;
                }
                $link = @$site->find('h2 a')[0] ? @$site->find('h2 a')[0]->href : '';
                $title = @$site->find('h2 a')[0] ? @$site->find('h2 a')[0]->text() : '';
                $description = @$site->find('.b_caption p')[0] ? @$site->find('.b_caption p')[0]->text() : '';
                if (!$description && @$site->find('.b_divsec')[0]) {
                    $description = @$site->find('.b_divsec')[0]->text();
                } elseif (@$site->find('.b_dList')[0]) {
                    $description = @$site->find('.b_dList')[0]->text();
                }
                $image = @$site->find('.b_circleImage')[0]->src;
                $imageSrc = getIDImagesBing($image) ? route('image', getIDImagesBing($image)) : null;
                $data = [
                    'title' => $title,
                    'link' => $link,
                    'description' => $description,
                    'img' => $imageSrc,
                ];
                if ($imageSrc) {
                    updateListImages(getSlug(strip_tags($title)), $imageSrc);
                }
                $more = $info = $ans = [];
                if (@$site->find('.b_dList')[0]) {
                    foreach (@$site->find('.b_dList')[0]->find('li') as $li) {
                        $more[] = $li->text();
                    }
                }

                if (@$site->find('.b_vlist2col')[0]) {
                    foreach (@$site->find('.b_vlist2col')[0]->find('ul') as $ul) {
                        foreach ($ul->find('li') as $l) {
                            $info[] = $l->text();
                        }
                    }
                }

                foreach (@$site->find('.b_listnav_item') as $item) {
                    $imgAns = @$item->find('.rms_img')[0]->src;
                    $title = @$item->find('.b_ans_caption')[0]->find('a')[0]->text();
                    $imgSrc = getIDImagesBing($imgAns) ? route('image', getIDImagesBing($imgAns)) : null;
                    $ans[] = [
                        'img' => $imgSrc,
                        'title' => $title,
                        'desc' => @$item->find('.b_listnav_item_caption')[0] ? @$item->find('.b_listnav_item_caption')[0]->text() : null,
                        'link' => @$item->find('a')[0] ? @$item->find('a')[0]->href : null
                    ];
                    if($title && $imgSrc){
                        updateListImages(getSlug(strip_tags($title)), $imgSrc);
                    }
                }

                //Video
                $siteVideo = @$site->find('.captionMediaCard')[0];
                if (@$siteVideo) {
                    $imgSiteVid = $siteVideo->find('img')[0]->getAttribute('data-src-hq');
                    $title = @$siteVideo->find('.vt22')[0] ? @$siteVideo->find('.vt22')[0]->getAttribute('aria-label') : null;
                    $linkVid = $siteVideo->find('link')[0]->href;
                    $data['video'] = [
                        'img' => getIDImagesBing($imgSiteVid) ? route('image', getIDImagesBing($imgSiteVid)) : null,
                        'title' => $title,
                        'link' => $linkVid,
                        'time' => @$siteVideo->find('.b_lRight .b_lLeft')[0] ? @$siteVideo->find('.b_lRight .b_lLeft')[0]->text() : null,
                    ];
                    if($title && strlen($title) > 0 && $linkVid){
                        updateListVideos(getSlug($title), getEmbedYoutube($linkVid));
                    }
                }

                $data['more'] = $more;
                $data['info'] = $info;
                $data['ans'] = [
                    'title' => @$site->find('.b_ans_stamp')[0] ? @$site->find('.b_ans_stamp')[0]->text() : null,
                    'items' => $ans
                ];
                $arrData[] = $data;
            }
            $videos = [];
            if (@$htmlDOMPage->find("#mmcar")[0]) {
                foreach ($htmlDOMPage->find("#mmcar")[0]->find('.slide') as $site) {
                    $imgVid = @$site->find('.rms_img')[0] ? @$site->find('.rms_img')[0]->src : null;
                    $linkVid = @$site->find('.mc_vhvc_th')[0] ? @$site->find('.mc_vhvc_th')[0]->vhk2 : null;
                    $linkVid = @$linkVid ? 'https://www.bing.com' . $linkVid : null;
                    $linkVid = iconv(mb_detect_encoding($linkVid, mb_detect_order(), true), "UTF-8", $linkVid);
                    $title = @$site->find('.mc_vtvc_title')[0] ? @$site->find('.mc_vtvc_title')[0]->text() : null;
                    $videos[] = [
                        'img' => getIDImagesBing($imgVid) ? route('image', getIDImagesBing($imgVid)) : null,
                        'title' => $title,
                        'video' => $linkVid,
                        'time' => @$site->find('.mc_bc.items')[0] ? @$site->find('.mc_bc.items')[0]->text() : null,
                        'channel' => @$site->find('.mc_vtvc_meta_row_channel')[0] ? @$site->find('.mc_vtvc_meta_row_channel')[0]->text() : null,
                        'viewed' => @$site->find('.meta_vc_content')[0] ? @$site->find('.meta_vc_content')[0]->text() : null,
                        'date' => @$site->find('.meta_pd_content')[0] ? @$site->find('.meta_pd_content')[0]->text() : null,
                    ];
                    if ($title && strlen($title) > 0) {
                        updateListVideos(getSlug($title), $linkVid);
                    }
                }
            } elseif (@$htmlDOMPage->find(".mc_vhvc")) {
                foreach ($htmlDOMPage->find(".mc_vhvc") as $site) {
                    $imgVid = @$site->find('.rms_img')[0] ? @$site->find('.rms_img')[0]->src : null;
                    $linkVid = @$site->find('.mc_vhvc_th')[0] ? @$site->find('.mc_vhvc_th')[0]->vhk2 : null;
                    $linkVid = @$linkVid ? 'https://www.bing.com' . $linkVid : null;
                    $linkVid = iconv(mb_detect_encoding($linkVid, mb_detect_order(), true), "UTF-8", $linkVid);
                    $title = @$site->find('.mc_vtvc_title')[0] ? @$site->find('.mc_vtvc_title')[0]->text() : null;
                    $videos[] = [
                        'img' => getIDImagesBing($imgVid) ? route('image', getIDImagesBing($imgVid)) : null,
                        'title' => $title,
                        'video' => $linkVid,
                        'time' => @$site->find('.mc_bc.items')[0] ? @$site->find('.mc_bc.items')[0]->text() : null,
                        'channel' => @$site->find('.mc_vtvc_meta_row_channel')[0] ? @$site->find('.mc_vtvc_meta_row_channel')[0]->text() : null,
                        'viewed' => @$site->find('.meta_vc_content')[0] ? @$site->find('.meta_vc_content')[0]->text() : null,
                        'date' => @$site->find('.meta_pd_content')[0] ? @$site->find('.meta_pd_content')[0]->text() : null,
                    ];

                    if ($title && strlen($title) > 0) {
                        updateListVideos(getSlug($title), $linkVid);
                    }
                }
            }
            $relatedKeyword = @$htmlDOMPage->find('.b_rrsr')[0] ? @$htmlDOMPage->find('.b_rrsr')[0] : (@$htmlDOMPage->find('.b_rs')[0] ?? []);
            if ($relatedKeyword) {
                foreach ($relatedKeyword->find(".b_vList")[0]->find('li') as $item) {
                    if (!@$item->find('a')) {
                        continue;
                    }
                    if (@$item->find('.b_suggestionText')[0]) {
                        $key = @$item->find('.b_suggestionText')[0]->outertext;
                        $key = str_replace(['<div>', '</div>'], '', $key);
                    } elseif (@$item->find('a>div')[0]) {
                        $key = @$item->find('a>div')[0]->outertext;
                        $key = str_replace(['<div>', '</div>'], '', $key);
                    } else {
                        $key = @$item->find('a')[0]->outertext;
                        $tags_to_strip = array("a");
                        foreach ($tags_to_strip as $tag) {
                            $key = preg_replace("/<\\/?" . $tag . "(.|\\s)*?>/", '', $key);
                        }
                    }
                    $key = str_replace('<div class="b_suggestionText">', '', $key);
                    if(!stringNeedInArray($key)){
                        continue;
                    }
                    $keywordRelated[] = $key;
                }
            }

            $question = @$htmlDOMPage->find('.b_ans.b_mop')[0];
            if ($question) {
                foreach ($question->find('.df_topAlAs') as $ask) {
                    $title = $ask->getAttribute('data-query');
                    $desc = $ask->find('.rwrl')[0]->text();
                    $questions[] = [
                        'title' => $title,
                        'description' => $desc,
                    ];
                }
            }

            try{
                $commonInfo = [
                    'title' => @$htmlDOMPage->find('.b_entityTitle')[0] ? @$htmlDOMPage->find('.b_entityTitle')[0]->text() : '',
                    'icon' => 'https://www.bing.com' . (@$htmlDOMPage->find('.b_float_img')[0] && @$htmlDOMPage->find('.b_float_img')[0]->find('img')[0] ? @$htmlDOMPage->find('.b_float_img')[0]->find('img')[0]->getAttribute('src') : ''),
                ];
            } catch (Exception $e){
                $commonInfo = [
                    'title' => '',
                    'icon' => '',
                ];
            }
            $commonInfo['icon'] = str_replace('qlt=20', 'qlt=100', $commonInfo['icon']);

            return [$arrData, $keywordRelated, $questions, $commonInfo, $phones, $videos];
        } catch (Exception $e){
            report($e);
            return [[], [], [], [], [], []];
        }
    }

    public function getDomainOrigin($domain)
    {
        $domain = trim($domain);

        $domain = str_replace(["https://", 'http://', 'http://www.', 'https://www.', 'www.'], '', $domain);
        $domain = strtolower(explode('/', $domain)[0]);

        if (isIP($domain)) {
            return $domain;
        }

        return getDomain($domain);
    }

    public function bingSearchEmail($domain)
    {
        try {
            $keyword = urlencode('"*@' . $domain . '" email contact');
            $url = "https://www.bing.com/search?q=$keyword&format=rss&count=100";

            $html = curlBing($url);
            $emails = self::extractEmailsFrom($domain, $html);

//            $emailFromUrl = self::getEmailFromUrl($domain);
//            $emails = array_merge($emails, array_values($emailFromUrl));
//            $emails = array_unique($emails);

            return $emails;
        } catch (\Exception $e) {
            return [];
            Log::info("Can't get email from $domain \n");
        }
    }

    public function getEmailFromUrl($domain)
    {
        $url = getUrlOriginFromDomain($domain);
        $cmd = "curl $url | grep @$domain";
        $html = shell_exec($cmd);
        $emails = self::extractEmailsFrom($domain, $html);
        return $emails;
    }

    function extractEmailsFrom($domain, $string)
    {
        preg_match_all(
            "/[a-z\d._%+-]+@\<em\>[a-z\d.-]+\.[a-z]{2,4}\b\<\/em\>/i",
            $string,
            $matches1
        );
        preg_match_all(
            "/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b/i",
            $string,
            $matches2
        );
        preg_match_all(
            "/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}+\.[a-z]{2,4}\b/i",
            $string,
            $matches3
        );
        $matches = array_merge($matches1, $matches2, $matches3);
        $emails = $arrCheck = [];
        foreach ($matches as $list) {
            if (empty($list)) {
                continue;
            }
            foreach ($list as $email) {
                $email = strtolower($email);
                $email = str_replace(['<em>', '</em>'], '', $email);
                if (isset($arrCheck[$email]) || strpos($email, $domain) === false) {
                    continue;
                }
                $arrCheck[$email] = 1;
                $emails[] = $email;
            }
        }

        return $emails;
    }

    public function getImagesFromBing($keyword, $limit = 40, $page = 0)
    {
        try {
            $urlOrigin = env('URL_BING_SEARCH_IMAGE');
            if (isDomain($keyword)) {
                $keyword = 'site:' . $keyword;
            } else {
                $keyword = Str::slug($keyword, '+');
            }
            $offset = $page != 0 ? $limit * ($page - 1) : 1;
            $url = str_replace('{{KEYWORD}}', $keyword, $urlOrigin);
            $url = str_replace('{{LIMIT}}', $limit, $url);
            $url = str_replace('{{OFFSET}}', $offset, $url);

            return self::getImagesBing($url);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function getImagesBing($url)
    {
        $html = curlBing($url);
        $htmlDOMPage = HtmlDomParser::str_get_html($html);
        if (empty($htmlDOMPage)) {
            return [];
        }
        $arrData = [];
        try {
            foreach ($htmlDOMPage->find("li.item.col") as $key => $item) {
                $title = @$item->find('.suggestion-title')[0]->plaintext;
                $title = preg_replace('/[^A-Za-z0-9]/', ' ', $title);
                $title = str_replace('  ', ' ', $title);

                $imgSrc = @$item->find('img')[0]->src;
                $arrData['suggest'][] = [
                    'title' => $title,
                    'src' => $imgSrc,
                ];
            }
        } catch (Exception $e) {
            $arrData['suggest'] = [];
        }

        try {
            foreach ($htmlDOMPage->find("ul.dgControl_list li") as $key => $item) {
                if (!@$item->find('.imgpt')[0]) {
                    continue;
                }
                $aElm = @$item->find('.imgpt')[0]->find('a')[0];
                $jsonData = html_entity_decode($aElm->getAttribute('m'));
                $jsonData = json_decode($jsonData, true);
                $arrData['images'][] = $jsonData;
            }
        } catch (Exception $e) {
            $arrData['images'] = [];
        }

        return $arrData;
    }
}
