<?php

namespace App\Console\Commands;

use App\Models\Companies;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SubmitURLBingCommand extends Command
{

    protected $signature = 'submit:bing_url';

    protected $description = 'Submit URL Bing API';

    protected $bingService;

    protected $siteUrl = 'https://www.globalcompanies.net/';

    protected $maxUrlInDay = 10;
    protected $maxUrlInTime = 10;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            $start = Carbon::parse('2021-01-26');
            $toDay = now();
            $daySubmit = $toDay->diff($start)->days;
            $offsetDay = $daySubmit * $this->maxUrlInTime;
            $numberTimeCallAPI = (int)ceil($this->maxUrlInDay / $this->maxUrlInTime);

            for ($i = 1; $i <= $numberTimeCallAPI; $i++) {
                $offset = $offsetDay + ($i * $this->maxUrlInTime);
                $companies = Companies::select('id', 'slug')->limit($this->maxUrlInTime)->offset($offset)->orderBy('id', 'asc')->get();
                $dataSitemap = self::getFullLink($companies);
                $data = [
                    'siteUrl' => $this->siteUrl,
                    'urlList' => $dataSitemap
                ];
                self::callAPI($data);

            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
    }

    public function callAPI($data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://ssl.bing.com/webmaster/api.svc/json/SubmitUrlbatch?apikey=75cd70e9ab9243b495aa9d570758d108",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 91d354f0-1caf-867a-d8fd-ca491092baff"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response. "\n";
        }
    }

    public function getFullLink($data)
    {
        $links = [];
        foreach ($data as $item) {
            $links[] = $this->siteUrl . $item->slug . '_' . $item->id;
        }
        return $links;
    }

}
