<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-language" content="en-us" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        @if(View::hasSection('title'))
            @yield('title')
        @else
            Find Phone Email Contact Support | SiteContact.Org
        @endif
    </title>
    <meta name="msvalidate.01" content="ED67C1C6D4C46B4C6B1A81CBAB9BE58B" />

    @if(View::hasSection('description'))
        <meta name="description" content="@yield('description')"/>
    @else
        <meta name="description" content="Find contact information, support phone number, email, service contact, technical support contact at companies, hospitals, schools from SiteContact.Org"/>
    @endif

    @php
        $path = request()->path() == '/' ? null : request()->path();
    @endphp
    <link rel="canonical" href="{{'https://www.sitecontact.org/' . $path}}" />
    <link rel="shortcut icon" href="{{asset('images/logo.png')}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

    <style>
        .hover-box{padding-left:10px;padding-bottom:15px;  padding-right:10px;background:#fff;border-radius:4px;border:1px solid #e9ecee;position:relative;overflow:hidden;text-align:left;transition:all .3s ease-in-out}.hover-box:hover{transform:translateY(-5px)}.hover-box h2{margin-top:0px;overflow:hidden;}.hover-box img{max-height:80px}
        .widget img {border: 2px solid #009cea;padding: 3px;border-radius: 5px;}.cursor-pointer:hover{cursor: pointer}.widget {margin-bottom: 15px;}h3.sidebarTitle {border: 1px solid #dfdfdf;padding: 7px 10px;margin: 0px; font-size: 22px;color: #fff !important;font-weight: bold;background-color: #007bff; border-color: #007bff;}.badge {color: #fff;}
        .phone_number,.email {color: #fff !important;font-weight: bold;font-size: 18px !important;}
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-YWW46HRST9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-YWW46HRST9');
    </script>
    @if(@$questions)
        <script type="application/ld+json">
            {!! getFAQ($questions) !!}
        </script>
    @endif
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{route('home')}}">SiteContact.Org</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link" href="{{route('images.index', 'a')}}">› Images</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('videos.index', 'a')}}">› Videos</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('keyword.index', 'microsoft-contact')}}">› Microsoft Contact</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('keyword.index', 'apple-contact')}}">› Apple Contact</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('keyword.index', 'apps-contact')}}">Apps</a>
                        <a class="dropdown-item" href="{{route('keyword.index', 'apple-contact')}}">Apple</a>
                        <a class="dropdown-item" href="{{route('keyword.index', 'software-contact')}}">Software</a>
                        <a class="dropdown-item" href="{{route('keyword.index', 'laptops-contact')}}">Laptops</a>
                        <a class="dropdown-item" href="{{route('keyword.index', 'iphone-contact')}}">Iphone</a>
                        <a class="dropdown-item" href="{{route('keyword.index', 'samsung-contact')}}">Samsung</a>
                        <a class="dropdown-item" href="{{route('keyword.index', 'amazon-contact')}}">Amazon</a>
                        <a class="dropdown-item" href="{{route('keyword.index', 'ebay-contact')}}">Ebay</a>
                    </div>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0" action="{{route('search')}}" method="post">
                {{csrf_field()}}
                <input type="text" class="form-control form-control-sm mr-sm-2" placeholder="Please put keyword here" name="keyword" value="{{@$keyword}}" size="30">
                <div class="input-group-btn">
                    <button class="btn btn-outline-info btn-sm my-2 my-sm-0" type="submit" name="submit"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>
            </form>>
        </div>
    </div>
</nav>
