<div id="right-sidebar">
    @php
        $recentlyKeywords = \Illuminate\Support\Facades\Cache::get('recently-keyword-contact') ?? [];
    @endphp
    @if(@$recentlyKeywords)
        <div class="widget m-panel">
            <h3 class="sidebarTitle list-group-heading text-info">Recently searched</h3>
            <div class="panel-content no-padding">
                <ul class="list-link list-unstyled">
                    @foreach($recentlyKeywords as $k => $item)
                        <p> &nbsp;&#8250; <a href="{{route('keyword.index', getSlug(strip_tags($item)))}}" @if($k > 3) rel="nofollow" @endif
                            title="{{ucfirst(getSlug(strip_tags($item), ' '))}}">{{ucfirst(getSlug(strip_tags($item), ' '))}}</a></p>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <div class="widget m-panel mt-25">
        <h3 class="sidebarTitle list-group-heading text-info">Phones number list</h3>
        <div class="panel-content no-padding">
            <ul class="list-group">
                @php $count =0 ; @endphp
                @foreach($dataList as $k => $item)
                    @php
                        $phones = getPhoneNumber($item['title'], $item['description']);
                        $emails = extractEmailsFrom($item['title'] . $item['description']);
                        if(!@$phones[0] && count($phones) == 0 && !@$emails[0]){
                            continue;
                        }
                    @endphp
                    @foreach(@$phones[0] as $key => $phone)
                        @php
                            $phone = \Illuminate\Support\Str::slug($phone);
                            if(strlen($phone) < 8){
                                continue;
                            }
                            $count++;
                        @endphp
                        <li class="list-group-item">
                            <span class="text-info" style="font-weight: bold;">{{$count}}.</span>
                            @if($phone)
                                <a href="#{{$k}}" title="{{$phone}}" rel="nofollow noopener noreferrer">
                                    <span style="font-weight: bold;">{{$phone}}</span> <sup><i class="fa fa-external-link" aria-hidden="true"></i></sup>
                                </a>
                            @endif
                            @if(@$emails[0])
                                <br>
                                <a href="mailto:{{@$emails[0]}}" rel="nofollow noopener noreferrer">{{@$emails[0]}}</a>
                            @endif
                        </li>
                    @endforeach
                @endforeach
            </ul>
        </div>
    </div>


    @if(@$videos)
        <div class="widget m-panel mt-25">
            <h3 class="sidebarTitle list-group-heading text-info"><i class="fa fa-video-camera"></i> Some Related Videos</h3>
            @foreach($videos as $item)
                @php
                    if(!@$item['video']){
                        continue;
                    }
                @endphp
                <iframe width="100%" height="207px" src="{{$item['video']}}"></iframe>
                <h4 class="text-primary">{!! $item['title'] !!}</h4>

                <p>{!! $item['date'] !!} <i class="fa fa-eye"></i> {!! $item['viewed'] !!}</p>
            @endforeach
        </div>
    @endif

    <div class="widget m-panel mt-25">
        <h3 class="sidebarTitle list-group-heading text-info">Popular Searched</h3>
        <div class="panel-content no-padding">
            @foreach($topKeywords as $k => $item)
                <p> &nbsp;&#8250; <a href="{{route('keyword.index', getSlug(strip_tags($item)))}}" @if($k > 3) rel="nofollow" @endif
                                     title="{{ucfirst(getSlug(strip_tags($item), ' '))}}">{{ucfirst(getSlug(strip_tags($item), ' '))}}</a></p>
            @endforeach
        </div>
    </div>

    @if(count($questions) > 0)
    <div class="widget m-panel mt-25">
        <h3 class="sidebarTitle list-group-heading text-info">FAQ {{ucwords($keyword)}}?</h3>
        <div class="row faq">
            @foreach($questions as $k => $item)
                <div class="col-md-12 mt-2 item">
                    <h3>{!! $item['title'] !!}</h3>
                    <p class="pl-2">{!! $item['description'] !!} @if(@$item['link'])
                        <a data-url="{{route('outlink', ['website' => $item['link']])}}" class="open-link" title="{!! $item['title'] !!}">Read more</a> @endif
                    </p>
                </div>
            @endforeach
        </div>
    </div>
    @endif
</div>
